# OmniOS

Operating Systems for all uses.
Customizable, Free, and Easy to use.
Combines the best of several Kernels Apps and Userspace Frameworks.

Base distros, Inspiration:
- Gentoo
- Debian / Ubuntu
- Neon KDE
- elementaryOS
- Nix OS
- LegacyOS